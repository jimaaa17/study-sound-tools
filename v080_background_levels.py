import queue
import sys

from matplotlib.ticker import FixedLocator, FixedFormatter
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft
import sounddevice as sd


channels = [1]
mapping = [c - 1 for c in channels]  # Channel numbers start with 1
downsample = 1
interval = 2000
window = 10000
device = None
q = queue.Queue()

try:
    device_info = sd.query_devices(device, "input")
    samplerate = device_info["default_samplerate"]
except Exception as e:
    print(e)
    raise


def audio_callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    # Fancy indexing with mapping creates a (necessary!) copy:
    q.put(indata[::downsample, mapping])


def update_plot(frame):
    """This is called by matplotlib for each plot update.

    Typically, audio callbacks happen more frequently than plot updates,
    therefore the queue tends to contain multiple blocks of audio data.

    """
    global plotdata
    while True:
        try:
            data = q.get_nowait()
        except queue.Empty:
            break
        shift = len(data)
        plotdata = np.roll(plotdata, -shift, axis=0)
        plotdata[-shift:, :] = data

    freqs, mags = time_to_freq(plotdata[:, 0], samplerate)
    lines[0].set_ydata(mags)
    return lines


def time_to_freq(
    timeseries,
    samplerate,
    bins_per_octave=24,
    low_cutoff=20,
):
    big_val = 1e6
    eps = 1e-3
    n_samples = timeseries.size
    n_octaves = np.log2(samplerate / low_cutoff)
    log_freqs = np.log10(
        eps + np.arange(n_samples) * 1000 * downsample / window)
    edges = np.arange(
        np.log10(low_cutoff),
        np.log10(samplerate / 2),
        np.log10(2) / bins_per_octave)
    mags = np.abs(fft(timeseries))
    log_mags = 10 * np.log10(mags + 1e-6)
    bin_centers = np.zeros(edges.size - 1)
    bin_mags = np.ones(edges.size - 1) * big_val
    for i_bin in range(int(edges.size - 1)):
        bin_centers[i_bin] = (edges[i_bin] + edges[i_bin + 1]) / 2
        i_bin_freqs = np.where(np.logical_and(
            log_freqs >= edges[i_bin], log_freqs < edges[i_bin + 1]))
        if i_bin_freqs[0].size > 0:
            bin_mags[i_bin] = np.mean(log_mags[i_bin_freqs])
    i_keep = np.where(bin_mags != big_val)
    bin_centers = bin_centers[i_keep]
    bin_mags = bin_mags[i_keep]

    return bin_centers, bin_mags


try:
    fig = plt.figure()
    ax = fig.add_axes((.13, .15, .81, .79))

    n_samples = int(window * samplerate / (1000 * downsample))
    plotdata = np.zeros((n_samples, 1))
    bin_centers, bin_mags = time_to_freq(plotdata, samplerate)
    lines = ax.plot(bin_centers, bin_mags)
    ax.axis((np.min(bin_centers), np.max(bin_centers), -20, 20))
    ax.set_xlabel("frequency (Hz)")
    ax.set_ylabel("magnitude (dB)")
    x_major_formatter = FixedFormatter([
        "20", "50", "100",
        "200", "500", "1k",
        "2k", "5k", "10k",
        "20k"])
    x_major_locator = FixedLocator([
        1.3, 1.7, 2,
        2.3, 2.7, 3,
        3.3, 3.7, 4,
        4.3])
    x_minor_locator = FixedLocator([
        1.48, 1.6, 1.78, 1.85, 1.9, 1.95,
        2.48, 2.6, 2.78, 2.85, 2.9, 2.95,
        3.48, 3.6, 3.78, 3.85, 3.9, 3.95])
    ax.xaxis.set_major_locator(x_major_locator)
    ax.xaxis.set_minor_locator(x_minor_locator)
    ax.xaxis.set_major_formatter(x_major_formatter)
    ax.grid(which="both")

    stream = sd.InputStream(
        device=device, channels=max(channels),
        samplerate=samplerate, callback=audio_callback)
    ani = FuncAnimation(fig, update_plot, interval=interval, blit=True)
    with stream:
        plt.show()

except Exception as e:
    print(e)
    raise
