import queue
import sys
from matplotlib.ticker import FixedLocator, FixedFormatter
from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as signal
try:
    import sounddevice as sd
except ModuleNotFoundError:
    print()
    print("sounddevice module isn't installed yet. Try")
    print("    python3 -m pip install sounddevice -U")
    print()
    print("If that doesn't work visit")
    print("https://e2eml.school/play_and_record_sounds.html")
    print()
    sys.exit()


def animate_frequency_response(
    # How many times should the visualization be updated per second?
    interval=20,
    # How many milliseconds of data should be included in creating each frame?
    # A longer window means that the curve will be less jumpy, but also that
    # It will respond to sounds more slowly.
    window=1000,
    # Setting device to None tells sounddevice to automatically figure out
    # which is the default device
    device=None,
):
    global audiodata
    try:
        # Query the input device to figure out what its sampling rate is
        # in samples per second.
        device_info = sd.query_devices(device, "input")
        samplerate = device_info["default_samplerate"]
    except Exception as e:
        print("Couldn't get the sample rate for the recording device")
        print(e)
        raise

    q = queue.Queue()
    # Get some fake data to initialize the plot with
    n_samples = int(window * samplerate / 1000)
    audiodata = np.zeros(n_samples)

    band_edges = [400, 500]
    filters = []
    for i_band in range(len(band_edges) - 1):
        filters.append(make_filter(
            band_edges[i_band], band_edges[i_band + 1], samplerate))

    def audio_callback(indata, frames, time, status):
        """
        This is called repeatedly from a separate thread.
        It helps read in data for each audio block.
        """
        # Fancy indexing creates a (necessary!) copy:
        q.put(indata[:, [0]])

    def update_plot(frame):
        """
        This is called by matplotlib for each plot update.

        Typically, audio callbacks happen more frequently than plot updates,
        therefore the queue tends to contain multiple blocks of audio data.
        """
        global audiodata
        while True:
            try:
                data = q.get_nowait()
            except queue.Empty:
                break
            shift = len(data)
            audiodata = np.roll(audiodata, -shift)
            audiodata[-shift:] = np.mean(data, 1)
        effective_window = 1000
        effective_window = np.minimum(audiodata.size, effective_window)
        freqs, mags = time_to_freq(audiodata[-effective_window:])

        lines[0].set_ydata(np.log2(np.array(mags) + 1e-6))
        return lines

    def time_to_freq(timeseries):
        """
        Convert a set of time sequence sound measurements to frequencies.
        """
        epsilon = 1e-10
        freqs = []
        mags = []
        for i_freq, band_filter in enumerate(filters):
            filtered_clip = signal.convolve(
                timeseries, band_filter, mode="valid")
            freqs.append(np.log10(band_edges[i_freq]))
            freqs.append(np.log10(band_edges[i_freq + 1]))
            mag = np.sum(np.abs(filtered_clip)) + epsilon
            mags.append(mag)
            mags.append(mag)
        return freqs, mags

    freqs, mags = time_to_freq(audiodata)
    fig, lines = create_plot(freqs, mags)

    try:
        # Set up the stream and kick off the collection
        stream = sd.InputStream(
            device=device, channels=1,
            samplerate=samplerate, callback=audio_callback)
        # This sets up the animation
        ani = FuncAnimation(fig, update_plot, interval=interval, blit=True)
        with stream:
            plt.show()

    except Exception as e:
        print(e)
        raise


def create_plot(freqs, mags):
    # Create and format the frequency domain plot
    fig = plt.figure()
    ax = fig.add_axes((.13, .15, .81, .79))
    lines = ax.plot(freqs, mags)
    ax.axis((np.log10(10), np.log10(45000), -15, 15))
    ax.set_xlabel("frequency (Hz)")
    ax.set_ylabel("magnitude (dB)")
    x_major_formatter = FixedFormatter([
        "20", "50", "100",
        "200", "500", "1k",
        "2k", "5k", "10k",
        "20k"])
    x_major_locator = FixedLocator([
        1.3, 1.7, 2,
        2.3, 2.7, 3,
        3.3, 3.7, 4,
        4.3])
    x_minor_locator = FixedLocator([
        1.48, 1.6, 1.78, 1.85, 1.9, 1.95,
        2.48, 2.6, 2.78, 2.85, 2.9, 2.95,
        3.48, 3.6, 3.78, 3.85, 3.9, 3.95])
    ax.xaxis.set_major_locator(x_major_locator)
    ax.xaxis.set_minor_locator(x_minor_locator)
    ax.xaxis.set_major_formatter(x_major_formatter)
    ax.grid(which="both")
    return fig, lines


def make_filter(cutoff_low, cutoff_high, samplerate):
    # Width of transition from pass band to stop band, Hz
    band_width = cutoff_high - cutoff_low
    half_transition = .25
    filter_size = 800
    edges = [
        0,
        cutoff_low - band_width * half_transition,
        cutoff_low + band_width * half_transition,
        cutoff_high - band_width * half_transition,
        cutoff_high + band_width * half_transition,
        0.5 * samplerate]
    band_pass_filter = signal.remez(
        filter_size,
        edges,
        [0, 1, 0],
        fs=samplerate)
    return band_pass_filter


if __name__ == "__main__":
    animate_frequency_response()
