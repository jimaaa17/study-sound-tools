import matplotlib.pyplot as plt
from cottonwood.experimental.heartbeat import Heartbeat
from cottonwood.structure import Structure
# from v290_system_microphone_block import SystemMicrophone as Microphone
from v380_system_mic_multiproc_block import SystemMicrophone as Microphone
from v320_featurizer_block import TimeSeriesFrequencyFeaturizer as Featurizer


pipeline = Structure()

pipeline.add(Microphone(n_samples=int(2**16)), "microphone")
pipeline.add(Featurizer(bins_per_octave=12), "featurizer")
pipeline.add(Heartbeat(interval_ms=100), "heartbeat")
pipeline.connect_sequence([
    "microphone",
    "featurizer",
    "heartbeat"
])

for i in range(10000):
    features = pipeline.forward_pass()
    if i == 0:
        plt.ion()
        plt.show()
        fig = plt.figure()
        ax = fig.gca()
        ax.set_ylim(-2, 2)
        line, = ax.plot(features)
    else:
        plt.pause(0.001)
        line.set_ydata(features)
