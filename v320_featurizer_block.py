import numpy as np
from numpy.fft import fft


class TimeSeriesFrequencyFeaturizer:
    """
    Converts a time series of data to a set of features based in the frequency
    domain.

    Creates both a set of amplitudes and their derivatives across
    a range of logarithmically spaced frequency bands.
    Also smoothes the feature values using a leaky integrator.

    The conversion from time domain to frequency domain happens
    octave-by-octave. The highest frequency that can be reliably represented
    is the Nyquist frequency--f/2 for a sampling frequency of f.
    On the first pass, the magnitudes of the frequency responses between
    f/4 and f/2 are calculated. The attribute bins_per_octave determines
    how many equal width (on a logarithmic scale) frequency bins are created
    over this octave. The fft is used to calculate the frueqency response,
    and the magnitudes of the responses are averaged in each bin.

    The second pass calculates the responses for the actave between
    f/8 and f/4 in a similar way. The third pass covers from f/16 to f/8.
    And so on.

    The same number of time series data samples are used at each octave.
    This number is the window_size. For a window_size of 1024, the final
    1024 data points are used to calculate the fft, even if there is a longer
    history of data to draw from. For the next octave, the data is
    downsampled by half, and the final 1024 data points are used to calculate
    the next octave's fft.

    The 50% downsampling for each octave means
    that to cover N octaves with a
    window_size of W, there needs to be at least W * 2**(N-1) data points
    in the original time series. Or, from a different angle, for a given
    window_size and a given length of time series data, there are only
    a certain number of octaves that can be supported. This number is
    calculated based on the data provided. The one knob you can turn when
    initializing the block is the bins_per_octave. The window_size required
    is directly related to the number of bins per octave. If you would
    like to cover more octaves (that is, cover lower frequencies) you can
    either provide a longer set of time series data or decrease the
    number of bins_per_octave.
    """
    def __init__(self, bins_per_octave=6, smoothing_factor=.5):
        """
        bins_per_octave (float) is the number of frequency bins per octave
        (doubling of frequency). For reference, there are 12 notes per octave
        in the chromatic scale of Western music. More notes per octave will
        result in a more finely sliced frequency breakdown.

        smoothing_factor (float) controls how quickly changes in the calculated
        frequency response are reflected in the running estimate.
        Rates close to one mean that new changes will take some time
        to show up, but the resulting frequency curve will vary smoothly.
        Rates closer to zero mean that new changes will be reflected more
        quickly, and can appear jumpier.
        """
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None

        # Several of these need to wait to be initialized until after the
        # length of the time series has been observed. They're assigned None
        # for now.
        self.bins_per_octave = int(bins_per_octave)
        self.bin_edges = None
        self.smoothing_factor = smoothing_factor
        self.last_magnitudes = None
        self.smoothed_magnitudes = None
        self.smoothed_derivatives = None
        self.n_octaves = None
        # The number of samples that should be included in running each fft.
        # This factor has been found through trial and error to work
        # pretty well. Going much shorter can leave some bins empty and
        # going much longer demands an unnecessarily large amount
        # of computation.
        self.window_size = 32 * self.bins_per_octave

    def initialize(self):
        n_samples = self.forward_in.size
        # Determine how many octaves of frequency
        self.n_octaves = int(np.log2(n_samples / self.window_size))
        if self.n_octaves == 0:
            print(f"Not enough time series data points {n_samples}")
            print(f"to support {self.bins_per_octave} bins_per_octave.")
            print("Either decrease bins_per_octave to at least " +
                  f"{int(n_samples / 32)}")
            print("or increase number of data points to at least " +
                  f"{self.bins_per_octave * 32}")
            raise RuntimeError(
                "Not enough time series data to support frequency conversion.")

        # Define the edges of the frequency bins.
        # These are evenly spaced on a log scale.
        # high_cutoff (float) is the highest frequency to consider in the plot.
        # The upper range of the frequencies to consider.
        # This needs to be less than half the sample rate.
        self.bin_edges = 10 ** np.linspace(
            np.log10(self.window_size / 4),
            np.log10(self.window_size / 2),
            self.bins_per_octave + 1)

        n_bins = self.n_octaves * self.bins_per_octave
        self.last_magnitudes = np.zeros(n_bins)
        self.smoothed_magnitudes = np.zeros(n_bins)
        self.smoothed_derivatives = np.zeros(n_bins)

    def __str__(self):
        # String representation for summarization
        str_parts = [
            "time series frequency featurizer",
            f"bins per octave: {self.bins_per_octave}",
            f"smoothing factor: {self.smoothing_factor}",
            f"number of octaves covered: {self.n_octaves}",
            ("number of frequency bins: " +
                f"{self.n_octaves * self.bins_per_octave}"),
            f"window size: {self.window_size}",
        ]
        return "\n".join(str_parts)

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        if self.smoothed_magnitudes is None:
            self.initialize()
        bin_magnitudes = []
        downsampled_data = np.copy(forward_in)
        for i_octave in range(self.n_octaves):
            # Convert the time series to frequencies using
            # the fast Fourier transform.
            data_to_use = downsampled_data[-self.window_size:]
            magnitudes = np.abs(fft(data_to_use))
            # Step through each frequency bin and aggregate responses
            octave_bin_magnitudes = []
            for i_bin in range(int(self.bin_edges.size - 1)):
                i_start = int(np.ceil(self.bin_edges[i_bin]))
                i_end = int(np.ceil(self.bin_edges[i_bin + 1]))
                # Find the average magnitudes of all the bin's frequencies
                octave_bin_magnitudes.append(
                    np.mean(magnitudes[i_start:i_end]))
            bin_magnitudes = octave_bin_magnitudes + bin_magnitudes

            downsampled_size = downsampled_data.size // 2
            old_data = downsampled_data[:2 * downsampled_size]
            downsampled_data = (old_data[0::2] + old_data[1::2]) / 2

        bin_magnitudes = np.array(bin_magnitudes)
        self.smoothed_magnitudes = (
            self.smoothed_magnitudes * self.smoothing_factor +
            bin_magnitudes * (1 - self.smoothing_factor))

        magnitude_difference = bin_magnitudes - self.last_magnitudes
        # self.smoothed_derivatives = (
        #     self.smoothed_derivatives * self.smoothing_factor +
        #     magnitude_difference * (1 - self.smoothing_factor))
        self.smoothed_derivatives = magnitude_difference
        self.last_magnitudes = bin_magnitudes

        self.forward_out = np.concatenate((
            self.smoothed_magnitudes.copy(),
            self.smoothed_derivatives.copy()))

        return self.forward_out

    def backward_pass(self, backward_in):
        """
        For now, don't try to reconstruct a time series from the frequency
        magnitudes. Just pass through any backward pass info.
        """
        self.backward_in = backward_in
        self.backward_out = backward_in
        return self.backward_out
