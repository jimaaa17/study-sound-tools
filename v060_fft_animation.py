import queue
import sys

from matplotlib.animation import FuncAnimation
import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft
import sounddevice as sd


channels = [1]
mapping = [c - 1 for c in channels]  # Channel numbers start with 1
downsample = 1
interval = 100
low_cutoff = 20
window = 100
device = None
q = queue.Queue()


def audio_callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    # Fancy indexing with mapping creates a (necessary!) copy:
    q.put(indata[::downsample, mapping])


def update_plot(frame):
    """This is called by matplotlib for each plot update.

    Typically, audio callbacks happen more frequently than plot updates,
    therefore the queue tends to contain multiple blocks of audio data.

    """
    global plotdata
    global i_keep
    while True:
        try:
            data = q.get_nowait()
        except queue.Empty:
            break
        shift = len(data)
        plotdata = np.roll(plotdata, -shift, axis=0)
        plotdata[-shift:, :] = data
    mags = np.abs(fft(plotdata[:, 0]))
    # Cut in half
    mags = mags[:int(mags.size / 2)]
    # Trim off low frequencies
    mags = mags[i_keep]
    lines[0].set_ydata(np.log2(mags + 1e-6))
    return lines


try:
    device_info = sd.query_devices(device, "input")
    samplerate = device_info["default_samplerate"]

    n_samples = int(window * samplerate / (1000 * downsample))

    fig, ax = plt.subplots()
    time = np.arange(n_samples) / samplerate
    freqs = np.arange(n_samples) * 1000 * downsample / window

    # Cut in half
    freqs = freqs[:int(n_samples / 2)]
    i_keep = np.where(freqs > low_cutoff)
    freqs = freqs[i_keep]
    plotdata = np.zeros((n_samples, 1))
    initial_data = np.zeros((freqs.size, 1))
    lines = ax.plot(freqs, initial_data)

    ax.axis((np.min(freqs), np.max(freqs), -6, 6))
    ax.set_yticks([0])
    ax.yaxis.grid(True)
    ax.tick_params(bottom=False, top=False, labelbottom=False,
                   right=False, left=False, labelleft=False)
    fig.tight_layout(pad=0)

    stream = sd.InputStream(
        device=device, channels=max(channels),
        samplerate=samplerate, callback=audio_callback)
    ani = FuncAnimation(fig, update_plot, interval=interval, blit=True)
    with stream:
        plt.show()
except Exception as e:
    print(e)
    raise
