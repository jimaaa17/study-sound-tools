import sys
from matplotlib.ticker import FixedLocator, FixedFormatter
import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft
import scipy.signal as signal
try:
    import sounddevice as sd
except ModuleNotFoundError:
    print()
    print("sounddevice module isn't installed yet. Try")
    print("    python3 -m pip install sounddevice -U")
    print()
    print("If that doesn't work visit")
    print("https://e2eml.school/play_and_record_sounds.html")
    print()
    sys.exit()


def record_and_filter(
    # Length of audio snippet to record and filter
    duration=3,
    # Setting device to None tells sounddevice to automatically figure out
    # which is the default device
    device=None,
    # The number of samples to neglect at the beginning to recording.
    # There can be weird blips (at least on my system)
    # if you don't chop the front.
    n_trim=10000,
):
    try:
        # Query the input device to figure out what its sampling rate is
        # in samples per second.
        device_info = sd.query_devices(device, "input")
        samplerate = device_info["default_samplerate"]
    except Exception as e:
        print("Couldn't get the sample rate for the recording device")
        print(e)
        raise

    n_samples = int(duration * samplerate)
    n_octaves = 9
    bands_per_octave = 3
    band_edges = 10000 * 2 ** np.linspace(0, 1, bands_per_octave + 1)
    filters = []
    for i_band in range(len(band_edges) - 1):
        filters.append(make_filter(
            band_edges[i_band], band_edges[i_band + 1], samplerate))
    raw_clip = sd.rec(n_trim + n_samples, channels=2, blocking=True)
    clip = np.mean(raw_clip[n_trim:, :], 1)

    effective_samplerate = samplerate
    resampled_clip = np.copy(clip)
    for i_octave in range(n_octaves):
        for band_filter in filters:
            filtered_clip = signal.convolve(
                resampled_clip, band_filter, mode="valid")
            expanded_clip = np.copy(filtered_clip)
            for _ in range(i_octave):
                new_clip = np.zeros(expanded_clip.size * 2)
                new_clip[0::2] = expanded_clip
                new_clip[1::2] = expanded_clip
                expanded_clip = new_clip
            sd.play(expanded_clip, blocking=True)
            plot_freq_response(filtered_clip, effective_samplerate)
        effective_samplerate /= 2
        resampled_clip = resampled_clip[:2 * (resampled_clip.size // 2)]
        resampled_clip = (resampled_clip[0::2] + resampled_clip[1::2]) / 2


def make_filter(cutoff_low, cutoff_high, samplerate):
    # Width of transition from pass band to stop band, Hz
    band_width = cutoff_high - cutoff_low
    half_transition = .125
    filter_size = 400
    edges = [
        0,
        cutoff_low - band_width * half_transition,
        cutoff_low + band_width * half_transition,
        cutoff_high - band_width * half_transition,
        cutoff_high + band_width * half_transition,
        0.5 * samplerate]
    band_pass_filter = signal.remez(
        filter_size,
        edges,
        [0, 1, 0],
        fs=samplerate)
    return band_pass_filter


def plot_freq_response(clip, samplerate):
    bin_centers, bin_mags = time_to_freq(
        clip,
        samplerate=samplerate,
    )

    # Create and format the frequency domain plot
    fig = plt.figure()
    ax = fig.add_axes((.13, .15, .81, .79))
    ax.plot(bin_centers, bin_mags)
    ax.axis((np.min(bin_centers), np.max(bin_centers), -30, 30))
    ax.set_xlabel("frequency (Hz)")
    ax.set_ylabel("magnitude (dB)")
    x_major_formatter = FixedFormatter([
        "20", "50", "100",
        "200", "500", "1k",
        "2k", "5k", "10k",
        "20k"])
    x_major_locator = FixedLocator([
        1.3, 1.7, 2,
        2.3, 2.7, 3,
        3.3, 3.7, 4,
        4.3])
    x_minor_locator = FixedLocator([
        1.48, 1.6, 1.78, 1.85, 1.9, 1.95,
        2.48, 2.6, 2.78, 2.85, 2.9, 2.95,
        3.48, 3.6, 3.78, 3.85, 3.9, 3.95])
    ax.xaxis.set_major_locator(x_major_locator)
    ax.xaxis.set_minor_locator(x_minor_locator)
    ax.xaxis.set_major_formatter(x_major_formatter)
    ax.grid(which="both")
    plt.show()


def time_to_freq(
    timeseries,
    bins_per_octave=12,
    low_cutoff=10,
    samplerate=44100,
):
    """
    Convert a set of time sequence sound measurements to frequencies.

    bins_per_octave (float) is the number of frequency bins per octave
    (doubling of freqency). For reference, there are 12 notes per octave
    in the chromatic scale of Western music. More notes per octave will
    result in a more finely sliced frequency breakdown. Fewer notes
    per octave will make the frequency response look less jumpy.

    low_cutoff (float) is the lowest frequency to consider in the plot.
    Including frquencies that are too low leads to some very jumpy
    lines. You can counter this by increasing the window length.
    """
    big_val = 1e6
    eps = 1e-3
    n_samples = timeseries.size

    # Transform the frequencies to a log scale
    duration = n_samples / samplerate
    log_freqs = np.log10(eps + np.arange(n_samples) / duration)
    # Define the edges of the frequency bins.
    # These are evenly spaced on a log scale.
    # Bins at the higher end will include a lot of different frequencies.
    # Bins at the lower end will have few, some one, and some none.
    bin_edges = np.arange(
        np.log10(low_cutoff),
        np.log10(samplerate / 2),
        np.log10(2) / bins_per_octave)

    # Convert the time series to frequencies using
    # the fast Fourier transform.
    energies = np.abs(fft(timeseries))
    # Convert energies at each frequency to decibels
    decibels = 10 * np.log10(energies + 1e-6)
    bin_centers = np.zeros(bin_edges.size - 1)
    # Initialize decibels to an easy-to-identify default
    bin_decibels = np.ones(bin_edges.size - 1) * big_val
    # Step through each frequency bin and aggregate responses
    for i_bin in range(int(bin_edges.size - 1)):
        bin_centers[i_bin] = (bin_edges[i_bin] + bin_edges[i_bin + 1]) / 2
        # Find all the frequencies that fall within the bin
        i_bin_freqs = np.where(np.logical_and(
            log_freqs >= bin_edges[i_bin],
            log_freqs < bin_edges[i_bin + 1]))
        # Find the average decibels of all the bin's frequencies
        if i_bin_freqs[0].size > 0:
            bin_decibels[i_bin] = np.mean(decibels[i_bin_freqs])

    # Find the bins that didn't collect any frequencies and remove them.
    i_keep = np.where(bin_decibels != big_val)
    bin_centers = bin_centers[i_keep]
    bin_decibels = bin_decibels[i_keep]

    return bin_centers, bin_decibels


if __name__ == "__main__":
    record_and_filter()
