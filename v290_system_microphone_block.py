import queue
import sys
import time
import numpy as np
try:
    import sounddevice as sd
except ModuleNotFoundError:
    print()
    print("sounddevice module isn't installed yet. Try")
    print("    python3 -m pip install sounddevice -U")
    print()
    print("If that doesn't work visit")
    print("https://e2eml.school/play_and_record_sounds.html")
    print()
    sys.exit()

# There's a recurring sementation fault for which this queue size is
# a hacky fix. I suspect the real way is to fix it is to handle audio
# in a separate process. It's possible that this thread is a little too
# busy to give the audio callback the attention it needs and it doesn't
# handle neglect gracefully.
q = queue.Queue(maxsize=10)


def audio_callback(indata, frames, time, status):
    """
    This is called repeatedly from a separate thread.
    It helps read in data for each audio block.
    """
    if status:
        print(status)
    # Explicit indexing with [:] ensures that a copy of the values are made,
    # rather than using a reference to the original variable, which
    # is likely to change.
    q.put(indata[:, [0]])


class SystemMicrophone:
    def __init__(
        self,
        device=None,
        n_samples=1024,
    ):
        self.forward_in = None
        self.forward_out = None
        self.backward_in = None
        self.backward_out = None
        self.n_samples = n_samples
        self.audio_data = np.zeros(self.n_samples)

        # Set up the audio stream connected to the microphone.
        # Setting device to None ensures that the default system microphone
        # will be selected.
        device = None
        # Query the input device to figure out what its sampling rate is
        # in samples per second.
        device_info = sd.query_devices(device, "input")
        samplerate = device_info["default_samplerate"]

        stream = sd.InputStream(
            device=device, channels=1,
            samplerate=samplerate, callback=audio_callback)
        stream.start()

    def __str__(self):
        # String representation for summarization
        return("system microphone stream")

    def get_data(self):
        while True:
            try:
                data = q.get_nowait()
            except queue.Empty:
                break
            shift = data.size
            self.audio_data = np.roll(self.audio_data, -shift)
            self.audio_data[-shift:] = data.ravel()

    def forward_pass(self, forward_in):
        self.forward_in = forward_in
        self.get_data()
        self.forward_out = np.copy(self.audio_data)
        return self.forward_out

    def backward_pass(self, backward_in):
        self.backward_in = backward_in
        return self.backward_out


if __name__ == "__main__":
    # Test basic functionality of the block
    # by generating some data samples.
    test_block = SystemMicrophone(n_samples=8000)
    for _ in range(10):
        time.sleep(.1)
        print(test_block.forward_pass(None))
    print(test_block.forward_pass(None).shape)
